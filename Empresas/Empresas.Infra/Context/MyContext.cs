﻿using Empresas.Domain.Entities;
using Empresas.Infra.Map;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Empresas.Infra.Context
{
    public class MyContext: DbContext
    {
        public DbSet<Investor> Investor { get; set; }
        public DbSet<Enterprise> Enterprise { get; set; }
        public DbSet<EnterpriseType> EnterpriseType { get; set; }


        public MyContext(DbContextOptions<MyContext> options) : base(options) 
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new InvestorMap());
            modelBuilder.ApplyConfiguration(new EnterpriseMap());
            modelBuilder.ApplyConfiguration(new EnterpriseTypeMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
