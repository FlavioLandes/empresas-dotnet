﻿using Empresas.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Empresas.Infra.Map
{
    public class EnterpriseMap : IEntityTypeConfiguration<Enterprise>
    {
        public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.EmailEnterprise).HasMaxLength(255);
            builder.Property(e => e.Facebook).HasMaxLength(255);
            builder.Property(e => e.Twitter).HasMaxLength(255);
            builder.Property(e => e.Linkedin).HasMaxLength(255);
            builder.Property(e => e.Phone).HasMaxLength(30);
            builder.Property(e => e.EnterpriseName).HasMaxLength(255);
            builder.Property(e => e.Photo).HasMaxLength(255);
            builder.Property(e => e.Description).HasMaxLength(1000);
            builder.Property(e => e.City).HasMaxLength(255);
            builder.Property(e => e.Country).HasMaxLength(255);

            builder.HasOne(e => e.EnterpriseType).WithMany(et => et.Enterprises).HasForeignKey(e => e.EnterpriseTypeId);
        }
    }
}
