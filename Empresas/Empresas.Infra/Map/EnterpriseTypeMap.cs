﻿using Empresas.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Empresas.Infra.Map
{
    public class EnterpriseTypeMap : IEntityTypeConfiguration<EnterpriseType>
    {
        public void Configure(EntityTypeBuilder<EnterpriseType> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.EnterpriseTypeName).HasMaxLength(255);
        }
    }
}
