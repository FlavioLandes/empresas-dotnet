﻿using Empresas.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Empresas.Infra.Map
{
    public class InvestorMap : IEntityTypeConfiguration<Investor>
    {
        public void Configure(EntityTypeBuilder<Investor> builder)
        {
            builder.HasKey(i => i.Id);

            builder.Property(i => i.InvestorName).HasMaxLength(255);
            builder.Property(i => i.Email).HasMaxLength(255);
            builder.Property(i => i.Password).HasMaxLength(255);
            builder.Property(i => i.City).HasMaxLength(255);
            builder.Property(i => i.Country).HasMaxLength(255);
            builder.Property(i => i.Photo).HasMaxLength(255);
        }
    }
}
