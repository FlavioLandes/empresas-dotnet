﻿using Empresas.Infra.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace Empresas.Infra.Repositories
{
    public class BaseRepository
    {
        protected readonly MyContext _context;

        public BaseRepository(MyContext context)
        {
            _context = context;
        }
    }
}
