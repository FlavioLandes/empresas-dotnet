﻿using Empresas.Domain.Entities;
using Empresas.Domain.Interfaces.Repositories;
using Empresas.Infra.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresas.Infra.Repositories
{
    public class EnterpriseRepository : BaseRepository, IEnterpriseRepository
    {
        public EnterpriseRepository(MyContext context) : base(context) 
        {
        }


        public async Task<IList<Enterprise>> GetMyEnterprises()
        {
            return await _context.Enterprise.Include(e => e.EnterpriseType).Where(e => e.OwnEnterprise).ToListAsync();
        }

        public async Task<IList<Enterprise>> GetAllEnterprises()
        {
            return await _context.Enterprise.Include(e => e.EnterpriseType).ToListAsync();
        }

        public async Task<Enterprise> GetEnterpriseById(long id)
        {
            return await _context.Enterprise.Include(e => e.EnterpriseType).Where(e => e.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IList<Enterprise>> GetEnterprisesByFilter(long enterpriseTypeId, string enterpriseName)
        {
            var iQueryAble = _context.Enterprise.AsQueryable();

            if (enterpriseTypeId > 0)
                iQueryAble = iQueryAble.Where(e => e.EnterpriseTypeId == enterpriseTypeId);

            if (!string.IsNullOrEmpty(enterpriseName))
                iQueryAble = iQueryAble.Where(e => e.EnterpriseName.ToLower().Contains(enterpriseName.ToLower()));

            iQueryAble = iQueryAble.Include(e => e.EnterpriseType);

            return await iQueryAble.ToListAsync();
        }
    }
}
