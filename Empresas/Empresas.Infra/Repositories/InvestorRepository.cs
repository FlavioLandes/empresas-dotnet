﻿using Empresas.Domain.Entities;
using Empresas.Domain.Interfaces.Repositories;
using Empresas.Infra.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresas.Infra.Repositories
{
    public class InvestorRepository : BaseRepository, IInvestorRepository
    {

        public InvestorRepository(MyContext context): base(context) { }

        public async Task<Investor> GetByEmailAndPassword(string email, string password)
        {
            return await _context.Investor.Where(x => x.Email == email && x.Password == password).FirstOrDefaultAsync();
        }

        public async Task Update(Investor investor)
        {
            _context.Investor.Update(investor);
            await _context.SaveChangesAsync();
        }
    }
}
