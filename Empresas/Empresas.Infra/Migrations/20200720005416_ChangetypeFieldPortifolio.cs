﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Infra.Migrations
{
    public partial class ChangetypeFieldPortifolio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "PortfolioValue",
                table: "Investor",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<double>(
                name: "Balance",
                table: "Investor",
                nullable: false,
                oldClrType: typeof(long));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "PortfolioValue",
                table: "Investor",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<long>(
                name: "Balance",
                table: "Investor",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
