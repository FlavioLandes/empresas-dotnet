﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Infra.Migrations
{
    public partial class ChangetypeFieldEnterpriseSharePrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "SharePrice",
                table: "Enterprise",
                nullable: false,
                oldClrType: typeof(long));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "SharePrice",
                table: "Enterprise",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
