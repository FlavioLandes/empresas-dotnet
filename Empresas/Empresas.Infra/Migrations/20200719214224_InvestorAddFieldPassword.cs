﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Infra.Migrations
{
    public partial class InvestorAddFieldPassword : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Investor",
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "Investor");
        }
    }
}
