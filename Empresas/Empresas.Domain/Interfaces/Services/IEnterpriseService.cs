﻿using Empresas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Empresas.Domain.Interfaces.Services
{
    public interface IEnterpriseService
    {
        Task<IList<Enterprise>> GetMyEnterprises();
        Task<IList<Enterprise>> GetAllEnterprises();
        Task<IList<Enterprise>> GetEnterprisesByFilter(long enterpriseTypeId, string enterpriseName);
        Task<Enterprise> GetEnterpriseById(long id);

    }
}
