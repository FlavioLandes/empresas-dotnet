﻿using Empresas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Empresas.Domain.Interfaces.Services
{
    public interface IInvestorService
    {
        Task<UserLogin> Login(string email, string password);
    }
}
