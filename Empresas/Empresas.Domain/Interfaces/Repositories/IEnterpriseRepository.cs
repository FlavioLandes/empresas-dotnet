﻿using Empresas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Empresas.Domain.Interfaces.Repositories
{
    public interface IEnterpriseRepository
    {
        Task<IList<Enterprise>> GetMyEnterprises();
        Task<IList<Enterprise>> GetAllEnterprises();
        Task<IList<Enterprise>> GetEnterprisesByFilter(long enterpriseTypeId, string enterpriseName);
        Task<Enterprise> GetEnterpriseById(long id);
    }
}
