﻿using Empresas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Empresas.Domain.Interfaces.Repositories
{
    public interface IInvestorRepository
    {
        Task<Investor> GetByEmailAndPassword(string email, string password);
        Task Update(Investor investor);
    }
}
