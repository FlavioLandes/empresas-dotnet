﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresas.Domain.Entities
{
    public class Investor
    {
        public long Id { get; set; }
        public string InvestorName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public double Balance { get; set; }
        public string Photo { get; set; }
        public double PortfolioValue { get; set; }
        public bool FirstAccess { get; set; }
        public bool SuperAngel { get; set; }


        public Investor()
        {
            FirstAccess = true;
        }
    }
}
