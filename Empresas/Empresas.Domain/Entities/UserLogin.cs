﻿using System.Collections.Generic;

namespace Empresas.Domain.Entities
{
    public class UserLogin
    {
        public long Id { get; set; }
        public string InvestorName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public double Balance { get; set; }
        public string Photo { get; set; }
        public double PortfolioValue { get; set; }
        public bool FirstAccess { get; set; }
        public bool SuperAngel { get; set; }

        public Portfolio Portfolio { get; set; }


        public TokenInfo TokenInfo { get; set; }

        public string Client { get; set; }
        public string UId { get; set; }
    }

    public class Portfolio
    {
        public long EnterprisesNumber { get; set; }
        public ICollection<Enterprise> Enterprises { get; set; }
    }

    public class TokenInfo
    {
        public string AccessToken { get; set; }
        public string TokenType { get; set; }
        public long Expiry { get; set; }
    }

}
