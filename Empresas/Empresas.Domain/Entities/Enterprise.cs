﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresas.Domain.Entities
{
    public class Enterprise
    {
        public long Id { get; set; }
        public string EmailEnterprise { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        public bool OwnEnterprise { get; set; }
        public string EnterpriseName { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public long Value { get; set; }
        public double SharePrice { get; set; }

        public long EnterpriseTypeId { get; set; }
        public EnterpriseType EnterpriseType { get; set; }
    }
}
