﻿using System.Collections.Generic;

namespace Empresas.Domain.Entities
{
    public class EnterpriseType
    {
        public long Id { get; set; }
        public string EnterpriseTypeName { get; set; }

        public ICollection<Enterprise> Enterprises { get; set; }
    }
}
