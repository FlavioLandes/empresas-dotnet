﻿using Empresas.Domain.Entities;
using Empresas.Domain.Interfaces.Repositories;
using Empresas.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Net.Http.Headers;
using System.Linq;

namespace Empresas.Domain.Services
{
    public class InvestorService : IInvestorService
    {
        private readonly IInvestorRepository _investorRepository;
        private readonly IEnterpriseService _enterpriseService;


        public InvestorService(IInvestorRepository investorRepository, IEnterpriseService enterpriseService)
        {
            _investorRepository = investorRepository;
            _enterpriseService = enterpriseService;
        }


        public async Task<UserLogin> Login(string email, string password)
        {
            string passwordMd5 = GetMd5Hash(password);

            var investor = await _investorRepository.GetByEmailAndPassword(email, passwordMd5);

            if (investor == null)
                return null;

            var myEnterprises = await _enterpriseService.GetMyEnterprises();


            var userLogin = new UserLogin
            {
                Id = investor.Id,
                InvestorName = investor.InvestorName,
                Email = investor.Email,
                City = investor.City,
                Country = investor.Country,
                Balance = investor.Balance,
                Photo = investor.Photo,
                PortfolioValue = investor.PortfolioValue,
                FirstAccess = investor.FirstAccess,
                SuperAngel = investor.SuperAngel,
                TokenInfo = GenerateToken(investor),
                Client = Guid.NewGuid().ToString(),
                UId = investor.Email,
                Portfolio = new Portfolio
                {
                    Enterprises = myEnterprises,
                    EnterprisesNumber = myEnterprises.Count
                }
            };


            if (investor.FirstAccess)
            {
                investor.FirstAccess = false;
                await _investorRepository.Update(investor);
            }


            return userLogin;
        }

        private TokenInfo GenerateToken(Investor investor)
        {
            var secretKey = Encoding.ASCII.GetBytes("TestEmpresasIoasys");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, investor.Id.ToString()),
                    new Claim(ClaimTypes.Role, "Investor")
                }),

                Expires = DateTime.UtcNow.AddHours(4),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);


            var tokenInfo = new TokenInfo
            {
                AccessToken = tokenHandler.WriteToken(token),
                Expiry = (long)DateTime.UtcNow.AddHours(4).Subtract(DateTime.UtcNow).TotalSeconds,
                TokenType = "Bearer"
            };


            return tokenInfo;
        }


        /// <summary>
        /// Convert string to Md5. // Reference: https://docs.microsoft.com/pt-br/dotnet/api/system.security.cryptography.md5?view=netframework-4.8
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Md5</returns>
        private string GetMd5Hash(string input)
        {
            using (var md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }
    }
}
