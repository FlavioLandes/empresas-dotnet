﻿using Empresas.Domain.Entities;
using Empresas.Domain.Interfaces.Repositories;
using Empresas.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Empresas.Domain.Services
{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        public EnterpriseService(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }

        public async Task<IList<Enterprise>> GetAllEnterprises()
        {
            return await _enterpriseRepository.GetAllEnterprises();
        }

        public async Task<Enterprise> GetEnterpriseById(long id)
        {
            return await _enterpriseRepository.GetEnterpriseById(id);
        }

        public async Task<IList<Enterprise>> GetEnterprisesByFilter(long enterpriseTypeId, string enterpriseName)
        {
            return await _enterpriseRepository.GetEnterprisesByFilter(enterpriseTypeId, enterpriseName);
        }

        public async Task<IList<Enterprise>> GetMyEnterprises()
        {
            return await _enterpriseRepository.GetMyEnterprises();
        }
    }
}
