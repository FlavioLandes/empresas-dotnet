﻿using AutoMapper;
using Empresas.Api.ViewModels;
using Empresas.Domain.Entities;
using Empresas.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Empresas.Api.Controllers
{
    //[Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseService _enterpriseService;
        private readonly IMapper _mapper;

        public EnterprisesController(IEnterpriseService enterpriseService, IMapper mapper)
        {
            _enterpriseService = enterpriseService;
            _mapper = mapper;
        }


        [Authorize(Roles = "Investor")]
        [HttpGet()]
        public async Task<IActionResult> GetList([FromQuery] long enterprise_types, [FromQuery] string name)
        {
            try
            {
                IList<Enterprise> enterprises = null;

                if (enterprise_types <= 0 && string.IsNullOrEmpty(name))
                    enterprises = await _enterpriseService.GetAllEnterprises();
                else
                    enterprises = await _enterpriseService.GetEnterprisesByFilter(enterprise_types, name);


                ConfigureResponseHeaders();

                var result = new
                {
                    enterprises = _mapper.Map<List<EnterpriseVM>>(enterprises),
                    success = true
                };

                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    error = ex.Message,
                    success = false
                };

                return StatusCode(500, result);
            }
        }



        [Authorize(Roles = "Investor")]
        [HttpGet("{id:long}")]
        public async Task<IActionResult> GetById(long id)
        {

            try
            {
                var enterprise = await _enterpriseService.GetEnterpriseById(id);

                if (enterprise == null)
                {
                    var result = new
                    {
                        status = "404",
                        error = "Not Found",
                        success = false
                    };

                    return StatusCode(404, result);
                }
                else
                {
                    ConfigureResponseHeaders();

                    var result = new
                    {
                        enterprise = _mapper.Map<EnterpriseVM>(enterprise),
                        success = true
                    };

                    return StatusCode(200, result);
                }
            }
            catch (Exception ex)
            {
                var result = new
                {
                    error = ex.Message,
                    success = false
                };

                return StatusCode(500, result);
            }
        }



        /// <summary>
        /// Configura os cabeçalhos de retorno conforme especificação do modelo do TESTE
        /// </summary>
        private void ConfigureResponseHeaders()
        {
            string accessToken = Request.Headers["access-token"];
            string client = Request.Headers["client"];
            string expiry = Request.Headers["expiry"];
            string uId = Request.Headers["uid"];

            Response.Headers.Add("access-token", accessToken);
            Response.Headers.Add("token-type", "Bearer");

            Response.Headers.Add("client", client);
            Response.Headers.Add("expiry", expiry);
            Response.Headers.Add("uid", uId);
        }
    }
}
