﻿using AutoMapper;
using Empresas.Api.ViewModels;
using Empresas.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresas.Api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IInvestorService _investorService;
        private readonly IMapper _mapper;

        public UsersController(IInvestorService investorService, IMapper mapper)
        {
            _investorService = investorService;
            _mapper = mapper;
        }


        [HttpPost("auth/sign_in")]
        public async Task<IActionResult> SignIn(UserLoginRequestVM userLoginVM)
        {
            try
            {
                var loginInfo = await _investorService.Login(userLoginVM.Email, userLoginVM.Password);

                if (loginInfo == null)
                {
                    var result = new
                    {
                        success = false,
                        errors = new List<string> { "Invalid login credentials. Please try again." }
                    };

                    return StatusCode(401, result);
                }
                else
                {
                    Response.Headers.Add("access-token", loginInfo.TokenInfo.AccessToken);
                    Response.Headers.Add("token-type", loginInfo.TokenInfo.TokenType);
                    Response.Headers.Add("client", loginInfo.Client);
                    Response.Headers.Add("expiry", loginInfo.TokenInfo.Expiry.ToString());
                    Response.Headers.Add("uid", loginInfo.UId);


                    var result = new
                    {
                        investor = _mapper.Map<UserLoginResponseVM>(loginInfo),
                        enterprise = loginInfo.Portfolio.Enterprises.FirstOrDefault(),
                        success = true
                    };

                    return StatusCode(200, result);
                }
            }
            catch (Exception ex)
            {
                var result = new
                {
                    error = ex.Message,
                    success = false
                };

                return StatusCode(500, result);
            }

        }
    }
}
