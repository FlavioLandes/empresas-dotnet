﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresas.Api.ViewModels
{
    public class UserLoginRequestVM
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
