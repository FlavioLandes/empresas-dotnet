﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresas.Api.ViewModels
{
    public class EnterpriseTypeVM
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("enterprise_type_name")]
        public string EnterpriseTypeName { get; set; }
    }
}
