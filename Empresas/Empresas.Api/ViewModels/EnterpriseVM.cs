﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresas.Api.ViewModels
{
    public class EnterpriseVM
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("email_enterprise")]
        public string EmailEnterprise { get; set; }

        [JsonProperty("facebook")]
        public string Facebook { get; set; }

        [JsonProperty("twitter")]
        public string Twitter { get; set; }

        [JsonProperty("linkedin")]
        public string Linkedin { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("own_enterprise")]
        public bool OwnEnterprise { get; set; }

        [JsonProperty("enterprise_name")]
        public string EnterpriseName { get; set; }

        [JsonProperty("photo")]
        public string Photo { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }

        [JsonProperty("share_price")]
        public double SharePrice { get; set; }


        public EnterpriseTypeVM EnterpriseType { get; set; }
    }
}
