﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresas.Api.ViewModels
{ 
    public class UserLoginResponseVM
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("investor_name")]
        public string InvestorName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("balance")]
        public double Balance { get; set; }

        [JsonProperty("photo")]
        public string Photo { get; set; }

        [JsonProperty("portfolio")]
        public PortfolioVM Portfolio { get; set; }

        [JsonProperty("portfolio_value")]
        public double PortfolioValue { get; set; }

        [JsonProperty("first_access")]
        public bool FirstAccess { get; set; }

        [JsonProperty("super_angel")]
        public bool SuperAngel { get; set; }

    }

    public class PortfolioVM
    {
        [JsonProperty("enterprises_number")]     
        public long EnterprisesNumber { get; set; }

        [JsonProperty("enterprises")]
        public ICollection<EnterpriseVM> Enterprises { get; set; }
    }
}
