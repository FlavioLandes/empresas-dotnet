﻿using Empresas.Api.ViewModels;
using Empresas.Domain.Entities;
using Empresas.Domain.Interfaces.Repositories;
using Empresas.Domain.Interfaces.Services;
using Empresas.Domain.Services;
using Empresas.Infra.Context;
using Empresas.Infra.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Empresas.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddApiVersioning(options =>
            {
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.ReportApiVersions = true;
            });


            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents()
                {
                    // Personalizacao para ler o token através do cabeçalho "access-token" em vez do padrão "Authorization"
                    // Também personalizacao para ler o token sem precisar que seja enviado junto com ele a palavra "Bearer"
                    // Nota: personalizacao feita por conta da exigencia da prova, onde é especificado qual cabecalho deve ser usado para enviar o token
                    OnMessageReceived = context =>
                    {
                        string header = context.HttpContext.Request.Headers["access-token"].ToString();

                        if (!string.IsNullOrEmpty(header))
                        {
                            context.Token = header.Trim();
                        }

                        return Task.CompletedTask;
                    },

                    // Personalizacao para retornar mensagem personalizada em caso de falha de autenticacao
                    // Nota: personalizacao feita por conta da exigencia da prova
                    OnAuthenticationFailed = context =>
                    {
                        context.Response.StatusCode = 401;
                        context.Response.ContentType = "application/json; charset=utf-8";

                        var message = new {
                            errors = new List<string> { "You need to sign in or sign up before continuing." }
                        };

                        return context.Response.WriteAsync(JsonConvert.SerializeObject(message));
                    }
                };

                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("TestEmpresasIoasys")),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });




            services.AddDbContext<MyContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddScoped<IInvestorRepository, InvestorRepository>();
            services.AddScoped<IEnterpriseRepository, EnterpriseRepository>();

            services.AddScoped<IInvestorService, InvestorService>();
            services.AddScoped<IEnterpriseService, EnterpriseService>();



            var config = new AutoMapper.MapperConfiguration(cfg =>
           {
               cfg.CreateMap<EnterpriseType, EnterpriseTypeVM>();
               cfg.CreateMap<Portfolio, PortfolioVM>();
               cfg.CreateMap<Enterprise, EnterpriseVM>();
               cfg.CreateMap<UserLogin, UserLoginResponseVM>();
           });

            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            app.UseAuthentication();

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
