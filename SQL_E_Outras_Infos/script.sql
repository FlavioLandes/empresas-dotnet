USE [master]
GO
/****** Object:  Database [Empresas]    Script Date: 20/07/2020 15:18:41 ******/
CREATE DATABASE [Empresas]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Empresas', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Empresas.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Empresas_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Empresas_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Empresas] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Empresas].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Empresas] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Empresas] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Empresas] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Empresas] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Empresas] SET ARITHABORT OFF 
GO
ALTER DATABASE [Empresas] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Empresas] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Empresas] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Empresas] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Empresas] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Empresas] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Empresas] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Empresas] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Empresas] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Empresas] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Empresas] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Empresas] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Empresas] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Empresas] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Empresas] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Empresas] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [Empresas] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Empresas] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Empresas] SET  MULTI_USER 
GO
ALTER DATABASE [Empresas] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Empresas] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Empresas] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Empresas] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Empresas] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Empresas] SET QUERY_STORE = OFF
GO
USE [Empresas]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 20/07/2020 15:18:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Enterprise]    Script Date: 20/07/2020 15:18:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enterprise](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EmailEnterprise] [nvarchar](255) NULL,
	[Facebook] [nvarchar](255) NULL,
	[Twitter] [nvarchar](255) NULL,
	[Linkedin] [nvarchar](255) NULL,
	[Phone] [nvarchar](30) NULL,
	[OwnEnterprise] [bit] NOT NULL,
	[EnterpriseName] [nvarchar](255) NULL,
	[Photo] [nvarchar](255) NULL,
	[Description] [nvarchar](1000) NULL,
	[City] [nvarchar](255) NULL,
	[Country] [nvarchar](255) NULL,
	[Value] [bigint] NOT NULL,
	[SharePrice] [float] NOT NULL,
	[EnterpriseTypeId] [bigint] NOT NULL,
 CONSTRAINT [PK_Enterprise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EnterpriseType]    Script Date: 20/07/2020 15:18:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnterpriseType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EnterpriseTypeName] [nvarchar](255) NULL,
 CONSTRAINT [PK_EnterpriseType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Investor]    Script Date: 20/07/2020 15:18:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Investor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[InvestorName] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[Country] [nvarchar](255) NULL,
	[Balance] [float] NOT NULL,
	[Photo] [nvarchar](255) NULL,
	[PortfolioValue] [float] NOT NULL,
	[FirstAccess] [bit] NOT NULL,
	[SuperAngel] [bit] NOT NULL,
	[Password] [nvarchar](255) NULL,
 CONSTRAINT [PK_Investor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200719204655_Inicial', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200719214224_InvestorAddFieldPassword', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200720005416_ChangetypeFieldPortifolio', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200720131917_ChangetypeFieldEnterpriseSharePrice', N'2.2.6-servicing-10079')
SET IDENTITY_INSERT [dbo].[Enterprise] ON 

INSERT [dbo].[Enterprise] ([Id], [EmailEnterprise], [Facebook], [Twitter], [Linkedin], [Phone], [OwnEnterprise], [EnterpriseName], [Photo], [Description], [City], [Country], [Value], [SharePrice], [EnterpriseTypeId]) VALUES (1, NULL, NULL, NULL, NULL, NULL, 0, N'AllRide', N'/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg', N'Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry', N'Santiago', N'Chile', 0, 50000, 2)
INSERT [dbo].[Enterprise] ([Id], [EmailEnterprise], [Facebook], [Twitter], [Linkedin], [Phone], [OwnEnterprise], [EnterpriseName], [Photo], [Description], [City], [Country], [Value], [SharePrice], [EnterpriseTypeId]) VALUES (2, NULL, NULL, NULL, NULL, NULL, 0, N'AQM S.A.', NULL, N'Cold Killer was discovered by chance in the ´90 s and developed by Mrs. Inés Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ', N'Maule', N'Chile', 0, 500000, 1)
INSERT [dbo].[Enterprise] ([Id], [EmailEnterprise], [Facebook], [Twitter], [Linkedin], [Phone], [OwnEnterprise], [EnterpriseName], [Photo], [Description], [City], [Country], [Value], [SharePrice], [EnterpriseTypeId]) VALUES (3, NULL, NULL, NULL, NULL, NULL, 0, N'urbanatika', NULL, N'Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry', N'Santiago', N'Chile', 0, 50000, 1)
SET IDENTITY_INSERT [dbo].[Enterprise] OFF
SET IDENTITY_INSERT [dbo].[EnterpriseType] ON 

INSERT [dbo].[EnterpriseType] ([Id], [EnterpriseTypeName]) VALUES (1, N'Agro')
INSERT [dbo].[EnterpriseType] ([Id], [EnterpriseTypeName]) VALUES (2, N'Software')
SET IDENTITY_INSERT [dbo].[EnterpriseType] OFF
SET IDENTITY_INSERT [dbo].[Investor] ON 

INSERT [dbo].[Investor] ([Id], [InvestorName], [Email], [City], [Country], [Balance], [Photo], [PortfolioValue], [FirstAccess], [SuperAngel], [Password]) VALUES (7, N'ioasys', N'testeapple@ioasys.com.br', N'BH', N'Brasil', 10000, NULL, 100000, 0, 1, N'ed2b1f468c5f915f3f1cf75d7068baae')
SET IDENTITY_INSERT [dbo].[Investor] OFF
/****** Object:  Index [IX_Enterprise_EnterpriseTypeId]    Script Date: 20/07/2020 15:18:41 ******/
CREATE NONCLUSTERED INDEX [IX_Enterprise_EnterpriseTypeId] ON [dbo].[Enterprise]
(
	[EnterpriseTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Enterprise]  WITH CHECK ADD  CONSTRAINT [FK_Enterprise_EnterpriseType_EnterpriseTypeId] FOREIGN KEY([EnterpriseTypeId])
REFERENCES [dbo].[EnterpriseType] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Enterprise] CHECK CONSTRAINT [FK_Enterprise_EnterpriseType_EnterpriseTypeId]
GO
USE [master]
GO
ALTER DATABASE [Empresas] SET  READ_WRITE 
GO
