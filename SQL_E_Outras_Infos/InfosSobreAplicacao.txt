Principais Tecnologias utilizadas:
* Asp.net Core 2.2
* Entity Framework Core 2.2.6 com abordagem Code First
* AutoMapper
* Autenticação com JWT (com envio de header e retornos personalizados de acordo com os requerimentos da prova)
* Visual Studio 2019

Principais Arquiteturas, Padrões e boas práticas utilizados:

* Arquitetura baseada no DDD
* Separação em camadas
* Repository
* Inversão de dependência (com Injeção de dependência)
* SOLID
* Clean Code
* Singleton
* Programação Assincrona
* Modelagem do banco feita pelo Code First com Entity Framework de modo que foram modelados os tipos de campos com o IEntityTypeConfiguration para serem mais otimizados(ex.: builder.Property(e => e.EmailEnterprise).HasMaxLength(255))


Notas para uso:

* Todas as chamadas são exatamente iguais ao modelo do postman exigido na prova (está no readme), bastando apenas alterar a porta utilizada (se for localhost seria https://localhost:44379)
* As credenciais de usuario e login também são as mesmas do readme da prova (Usuário de Teste: testeapple@ioasys.com.br   Senha de Teste : 12341234)

* Rodando o projeto em modo desenvolvimento apenas é preciso rodar os migrations através do comando Update-Database -v dentro do Package Manager Console do Visual Studio 2019
* A modelagem e dados do banco de dados estão no arquivo script.sql
* A senha armazenada foi criptografada em MD5 (durante o login o código compara a senha normal com a do banco, ou seja, encriptografa a normal e depois compara)
* Precisa ajustar a connection string para o novo banco (fica no appsettings.json)